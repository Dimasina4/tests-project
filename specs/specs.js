var form = require('../page_objects/form');
var list = require('../page_objects/list');
const formData = require('../fixtures/formData');

describe('User Registration Form & List of Users', function () {
    beforeAll(function () {
        browser.get('http://localhost:8080/TestAppExample/index');
    });
    it('test1 (formData.test1)', function () {
        var userData = [formData.test1.name, formData.test1.address, formData.test1.email];
        form.setName(userData[0]);
        form.setAddress(userData[1]);
        form.setEmail(userData[2]);
        form.clickAddBtn();
        expect(list.getLastUserData()).toEqual(userData);
        list.removeLastUser();
    });
    it('test2 (formData.test2)', function () {
        var userData1 = [formData.test2.name, formData.test2.address, formData.test2.email];
        var userData2 = [formData.test2.name.trim(), formData.test2.address.trim(), formData.test2.email.trim()];
        form.setName(userData1[0]);
        form.setAddress(userData1[1]);
        form.setEmail(userData1[2]);
        form.clickAddBtn();
        expect(list.getLastUserData()).toEqual(userData2);
        list.removeLastUser();
    });
    it('test3 (formData.test3)', function () {
        var userData = [formData.test3.name, formData.test3.address, formData.test3.email];
        form.setName(userData[0]);
        form.setAddress(userData[1]);
        form.setEmail(userData[2]);
        form.clickAddBtn();
        expect(list.getLastUserData()).toEqual(userData);
        list.removeLastUser();
    });
    it('test4 (not creating user with same name)', function () {
        var userData = [formData.test1.name, formData.test1.address, formData.test1.email];
        form.setName(userData[0]);
        form.setAddress(userData[1]);
        form.setEmail(userData[2]);
        form.clickAddBtn();
        list.getListLength().then(function (listLength1) {
            form.setName(userData[0]);
            form.setAddress(userData[1]);
            form.setEmail(userData[2]);
            form.clickAddBtn();
            expect(list.getListLength()).toBe(listLength1);
        })
    });
    it('test5 (cyrillic name, address)', function () {
        var userData = [formData.test5.name, formData.test5.address, formData.test5.email];
        form.setName(userData[0]);
        form.setAddress(userData[1]);
        form.setEmail(userData[2]);
        form.clickAddBtn();
        expect(list.getLastUserData()).toEqual(userData);
        list.removeLastUser();
    });
    it('test6 (name >= 3 valid)', function () {
        form.setName(formData.test6.name);
        expect(form.getNameFieldStatus()).toEqual('green');
    });
    it('test7 (name < 3 not valid)', function () {
        form.setName(formData.test7.name);
        expect(form.getNameFieldStatus()).toEqual('yellow');
    });
    it('test8 (name: clear text and blur)', function () {
        form.setName(formData.test6.name);
        form.setName('');
        expect(form.getNameFieldStatus()).toEqual('red');
    });


    // it('should work', function () {
    //     expect(list.getLastUserData()).toBe('asd');
    // });
});