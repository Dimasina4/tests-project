var Form = function () {
    //для каждого input gettext; settext; getcolor(отдельно) + isTrue/isFalse/isEmpty;
    //button: isEnabled;click
    getElementStatus = async function (element) {
        return element.getCssValue('background-color').then(function (res) {
            if (res === 'rgba(144, 238, 144, 1)') return 'green';
            else if (res === 'rgba(255, 255, 0, 1)') return 'yellow';
            else if (res ==='rgba(255, 0, 0, 1)') return 'red';
            else return 'white';
        });
    };

    // Inputs
    var nameField = element(by.model('controller.user.userName'));
    var addressField = element(by.model('controller.user.address'));
    var emailField = element(by.model('controller.user.email'));

    // Buttons
    var addBtn = element(by.css('#submit'));
    var resetBtn = element(by.css('#reset'));

    this.getName = function () {
        return nameField.getAttribute('value');
    };

    this.setName = function (text) {
        nameField.clear().sendKeys(text);
    };

    this.getNameFieldStatus = function () {
        return (getElementStatus(nameField));
    };

    this.getAddress = function () {
        return addressField.getAttribute('value');
    };

    this.setAddress = function (text) {
        addressField.clear().sendKeys(text);
    };

    this.getAddressFieldStatus = function () {
        return (getElementStatus(addressField));
    };

    this.getEmail = function () {
        return emailField.getAttribute('value');
    };

    this.setEmail = function (text) {
        emailField.clear().sendKeys(text);
    };

    this.getEmailFieldStatus = function () {
        return (getElementStatus(emailField));
    };

    this.clickAddBtn = function () {
        addBtn.click();
    };

    this.isAddBtnEnabled = function () {
        return addBtn.isEnabled();
    };

    this.clickResetBtn = function () {
        resetBtn.click();
    };

    this.isResetBtnEnabled = function () {
        return resetBtn.isEnabled();
    };
};

module.exports = new Form();