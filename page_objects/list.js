var List = function () {
    //получать массив пользователей, искать элемент по имени (возможно по другим полям) и возвращать весь элемент (содержащий все поля + кнопки)
    //функции: кол-во пользователей, нажатие кнопок для нужного пользователя (+ модал), ?получение всего списка имен/адресов/почт?,
    //?получение последнего пользователя в списке (последний добавленный)?, ?наличие пользователя по имени?

    getElementByName = function (name) {
        return usersList.filter(function (elem) {
            return elem.element(by.binding('u.userName')).getText().then(function (text) {
                return text === name;
            })
        }).first();
    };

    var usersList = element.all(by.css('tr.user-row'));
    var modalOk = element(by.css('#ok'));
    var modalCancel = element(by.css('#cancel'));

    this.clickEditByName = function (name) {
        return getElementByName(name).element(by.css('#edit')).click();
    };

    this.clickRemoveByName = function (name) {
        return getElementByName(name).element(by.css('#remove')).click();
    };

    this.clickRemoveById = function (id) {
        return usersList.get(id - 1).element(by.css('#remove')).click();
    };

    this.clickModalOk = function () {
        modalOk.click();
    };

    this.clickModalCancel = function () {
        modalCancel.click();
    };

    this.isModalLabelContainsText = function (name) {
        return element(by.css('.modal-dialog label.control-lable')).getText().then(function (text) {
            return text.includes(name);
        });
    };

    this.getListLength = function () {
        return usersList.then(function (items) {
            return items.length;
        });
    };

    this.getListOfUserNames = function () {
        return usersList.all(by.binding('u.userName')).getText();
    };

    this.getLastUserData = function () {
        var user = usersList.last();
        return user.element(by.binding('u.userName')).getText().then(function (a) {
            return user.element(by.binding('u.address')).getText().then(function (b) {
                return user.element(by.binding('u.email')).getText().then(function (c) {
                    return [a, b, c];
                })
            });
        });
    };

    this.removeLastUser = function () {
        usersList.last().element(by.css("#remove")).click();
        this.clickModalOk();
    };

    this.getUserData = function (userName) {
        var user = getElementByName(userName);
        return user.element(by.binding('u.userName')).getText().then(function (a) {
            return user.element(by.binding('u.address')).getText().then(function (b) {
                return user.element(by.binding('u.email')).getText().then(function (c) {
                    return [a, b, c];
                })
            });
        });
    };
};

module.exports = new List();